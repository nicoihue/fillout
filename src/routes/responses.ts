import express from 'express';
import { getFilteredResponses } from '../controllers/filteredResponses';

const router = express.Router();

router.get('/:formId/filteredResponses', getFilteredResponses);

export default router;