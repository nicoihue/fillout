import { Request, Response } from 'express';
import { filtersValidation } from '../validations/filters';
import { filterResponses, getResponses } from '../services/responses';

export const getFilteredResponses = async (req: Request, res: Response) => {
  const { formId } = req.params;
  const { filters } = req.query;

  const { success, msg, parsedFilters } = filtersValidation(filters);
  if (!success) {
    return res.status(400).json({ error: msg });
  }

  try {
    const submissions = await getResponses(formId);
    const filteredResponses = filterResponses(submissions, parsedFilters);
    res.status(200).json(filteredResponses);
  } catch (error) {
    res.status(500).json({ error: 'Error calling external endpoint' });
  }
};
