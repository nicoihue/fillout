import Joi from 'joi';
import { ResponseFiltersType } from '../types/filters';

export const filtersValidation = (
  filters: any,
): {
  success: boolean;
  msg?: string;
  parsedFilters: ResponseFiltersType;
} => {
  try {
    const parsedFilters: ResponseFiltersType = JSON.parse(filters);
    const filterSchema = Joi.object({
      id: Joi.string().required(),
      condition: Joi.string()
        .valid('equals', 'does_not_equal', 'greater_than', 'less_than')
        .required(),
      value: Joi.alternatives().try(Joi.number(), Joi.string()).required(),
    }).required();

    const filtersSchema = Joi.array().items(filterSchema).min(1).required();
    const { error } = filtersSchema.validate(parsedFilters);
    if (error) {
      return { success: false, msg: error.details[0].message, parsedFilters: [] };
    }
    return { success: true, parsedFilters };
  } catch (error) {
    console.error(error);
    return { success: true, msg: 'Error parsing filters', parsedFilters: [] };
  }
};
