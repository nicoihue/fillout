import axios from 'axios';
import { FilterClauseType, ResponseFiltersType } from '../types/filters';
import { operatorFunction } from '../utils/operators';
import { Submission } from '../types/submission';

export const filterResponses = (submissions: Submission[], filters: ResponseFiltersType) => {
  if (filters.length === 0) return submissions;

  const ids = new Set(filters.map((filter) => filter.id));
  const filteredSubmissions = submissions.filter((submission) => {
    let matches = true;
    let checkedFilters = 0;
    for (let x = 0; matches && x < submission.questions.length; x++) {
      const question = submission.questions[x];
      if (ids.has(question.id)) {
        const filt = filters.find((filter) => filter.id === question.id) as FilterClauseType;
        matches = operatorFunction(filt.condition)(question.value, filt.value);
        if (!matches) break;
        checkedFilters++;
      }
    }
    return matches && checkedFilters === filters.length;
  });

  return filteredSubmissions;
};

export const getResponses = async (formId: string) => {
  const { API_KEY, API_URL } = process.env;

  try {
    const { data } = await axios.get(`${API_URL}/${formId}/submissions`, {
      headers: {
        Authorization: `Bearer ${API_KEY}`,
      },
    });
    return data.responses;
  } catch (error) {
    console.error('Error calling external endpoint:');
    throw error;
  }
};
