import { Condition } from '../types/filters';

type OperatorFunction = (a: number | string, b: number | string) => boolean;

export const operatorFunction = (condition: Condition): OperatorFunction => {
  switch (condition) {
    case 'equals':
      return (a, b) => a === b;
    case 'does_not_equal':
      return (a, b) => a !== b;
    case 'greater_than':
      return (a, b) => a > b;
    case 'less_than':
      return (a, b) => a < b;
    default:
      return (a, b) => a === b;
  }
};
