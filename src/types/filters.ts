export type Condition = 'equals' | 'does_not_equal' | 'greater_than' | 'less_than';

export type FilterClauseType = {
  id: string;
  condition: Condition;
  value: number | string;
};

export type ResponseFiltersType = FilterClauseType[];
