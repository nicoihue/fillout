export type Submission = {
  submissionId: string;
  submissionTime: string;
  lastUpdatedAt: string;
  questions: {
    id: string;
    name: string;
    type: string;
    value: string | number;
  }[];
};
