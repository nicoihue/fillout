import { Condition } from '../../src/types/filters';
import { operatorFunction } from '../../src/utils/operators';

describe('operatorFunction', () => {
  it('should return a function that checks for equality', () => {
    const equals = operatorFunction('equals');
    expect(equals(5, 5)).toBe(true);
    expect(equals(5, 10)).toBe(false);
  });

  it('should return a function that checks for inequality', () => {
    const doesNotEqual = operatorFunction('does_not_equal');
    expect(doesNotEqual(5, 5)).toBe(false);
    expect(doesNotEqual(5, 10)).toBe(true);
  });

  it('should return a function that checks for greater than', () => {
    const greaterThan = operatorFunction('greater_than');
    expect(greaterThan(5, 5)).toBe(false);
    expect(greaterThan(10, 5)).toBe(true);
    expect(greaterThan(5, 10)).toBe(false);
  });

  it('should return a function that checks for less than', () => {
    const lessThan = operatorFunction('less_than');
    expect(lessThan(5, 5)).toBe(false);
    expect(lessThan(5, 10)).toBe(true);
    expect(lessThan(10, 5)).toBe(false);
  });

  it('should return a function that checks for equality by default', () => {
    const defaultOperator = operatorFunction('unknown' as Condition);
    expect(defaultOperator(5, 5)).toBe(true);
    expect(defaultOperator(5, 10)).toBe(false);
  });
});
