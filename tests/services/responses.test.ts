import { filterResponses } from '../../src/services/responses';
import { ResponseFiltersType } from '../../src/types/filters';
import { Submission } from '../../src/types/submission';

describe('filterResponses', () => {
  const submissions: Submission[] = [
    {
      submissionId: '1',
      submissionTime: '2022-03-01',
      lastUpdatedAt: '2022-03-01',
      questions: [
        { id: 'q1', name: 'Question 1', type: 'text', value: 'yes' },
        { id: 'q2', name: 'Question 2', type: 'text', value: 'yes' },
      ],
    },
    {
      submissionId: '2',
      submissionTime: '2022-03-02',
      lastUpdatedAt: '2022-03-02',
      questions: [{ id: 'q2', name: 'Question 2', type: 'text', value: 'no' }],
    },
  ];

  it('should filter submissions based on filters', () => {
    const filters: ResponseFiltersType = [
      { id: 'q1', condition: 'equals', value: 'yes' },
      { id: 'q2', condition: 'equals', value: 'yes' },
    ];
    const filteredSubmissions = filterResponses(submissions, filters);
    expect(filteredSubmissions.length).toBe(1);
    expect(filteredSubmissions[0].submissionId).toBe('1');
  });

  it('should return empty array if no submissions match filters', () => {
    const noMatchFilters: ResponseFiltersType = [{ id: 'q1', condition: 'equals', value: 'no' }];
    const filteredSubmissions = filterResponses(submissions, noMatchFilters);
    expect(filteredSubmissions.length).toBe(0);
  });
});
