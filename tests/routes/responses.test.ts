import request from 'supertest';
import app from '../../src';

describe('/:formId/filteredResponses', () => {
  const { FORM_ID } = process.env;
  const filters = [
    { id: 'bE2Bo4cGUv49cjnqZ4UnkW', condition: 'equals', value: 'Johnny' },
    { id: 'fFnyxwWa3KV6nBdfBDCHEA', condition: 'greater_than', value: 3 },
  ];
  const queryString = JSON.stringify(filters);

  it('happy path', async () => {
    const filters = [
      { id: 'bE2Bo4cGUv49cjnqZ4UnkW', condition: 'equals', value: 'Johnny' },
      { id: 'fFnyxwWa3KV6nBdfBDCHEA', condition: 'greater_than', value: 3 },
    ];

    const url = `/${FORM_ID}/filteredResponses?filters=${queryString}`;

    const response = await request(app).get(url);

    expect(response.status).toBe(200);
  });
  it('unhappy path', async () => {
    const invalidFormId = 'INVALID';

    const url = `/${invalidFormId}/filteredResponses?filters=${queryString}`;

    const response = await request(app).get(url);

    expect(response.status).toBe(500);
  });
});
